# Tips for Making Matcha Tea #

Introduction

In the traditional sense, Matcha has mainly been used in japan during their tea ceremony known as Chanoyu. The current situation however is that Matcha is being prepared and drunk in casual settings, with emphasis being on the iced Matcha, which has become the new favorite drink in Japan due to its refreshing and delicious attributes especially in the hot and sultry weather. Matcha in this case refers to a powdered form of green tea, predominantly found in japan, is commonly associated with intricate tea ceremonies whereby tea is whisked and consequently served in special tatami-floored teahouses. Currently, it is possible for anyone to enjoy the delicious Matcha tea in the kitchen by using a few ingredients and utensils, with a little less formality. Below are the directions one can follow in making the delicious bowl of Matcha to be enjoyed at the convenience of your kitchen.

Utensils

Tea bowl

In the traditional tea ceremony, selection of the appropriate is bowl is a necessity, especially in preparation of Matcha tea. It is imperative that the bowl is sturdy, adequately wide to allow one to whisk in it and pleasing to hold in your hands. One can use a French latte bowl or any other that is considered to be suitable.

The Whisk

Use of the traditional bamboo whisk which is also known as the Chasen is necessary for the preparation of Matcha. While one can also use an electric latte aerator, it is considered to have a different aesthetic experience. Use of a metal whisk is not recommendable and the fact that a bamboo whisk can be purchased from shops that sell Matcha tea or online for less than $15 making it a necessity to be able to enjoy the ideal Matcha tea.

Fine Mesh Sieve

The mesh is necessary to assist in the sifting of the tea.

The other components needed to be able to make the ideal Matcha tea include:

· High grade Matcha tea

· A bamboo scoop also known as a Chashaku although a spoon can be used

Method

· Using a strainer and the Matcha scoop, sift the Matcha tea into the bowl, moving the scoop over the powder gently back and forth and slowly pushing it through. Each step should be approached with a lot of care and mindfulness due to Matcha's delicate nature.

· Boil approximately 80 Mls of water and allow cooling for about 5 minutes before pouring it into a mug. It is imperative that boiling water is not poured straight on Matcha as it burns the delicate plant structure resulting into the loss of nutrients in the tea.

· Then, steadily whisk the Matcha and water together using the W and M pattern. In order to be able to produce froth, one needs to whist quickly so as to get air through the tea and produce bubbles. You should notice that the Matcha powder does not dissolve completely into the water but is suspended on in the water.

· Once there has been the formation of froth on top, one needs to gently drag the whisk ( chasen ) back and forth, popping any large bubbles. At this point no large bubbles should be present, just a smooth, consistent creamy layer of froth on top, presenting you with a nice, tight crema.

· At this point, you should just sit down and enjoy your tea treat. You should pick up your bowl in your hands and take small but quick sips of the delicious Matcha. Traditionally, your bowl should not be set down until you have drunk all the tea. Once done, traditions recommend that you examine your bowl, admitting its shape and glaze; it is optional though.

* [matchakyoto](https://matchakyoto.com/)

